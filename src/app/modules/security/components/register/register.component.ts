import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    username: new FormControl("flavian", [Validators.required]),
    password: new FormControl("Test1234=", [Validators.required, Validators.minLength(5)]),
    address: new FormControl("BlopStreet", [])
  })
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.registerForm.valid);
    console.log(this.registerForm.value);
  }

}
