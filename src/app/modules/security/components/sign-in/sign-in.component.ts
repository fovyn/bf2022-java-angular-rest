import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  username: string = "";
  password: string = "";

  constructor(
    private _auth: AuthService,
    private _session: SessionService,
    private _router: Router
    ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.username, this.password);
    this._auth.callLogin(this.username, this.password).subscribe(data => {
      console.log(data);
      this._session.login(data[0]);
    })
  }

}
