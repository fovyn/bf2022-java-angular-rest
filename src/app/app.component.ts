import { HttpClient, HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { SessionService } from './modules/security/services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bf2022-java-angularrest';
  user: any | null = null;

  constructor(private _http: HttpClient, private _session: SessionService) {
  }

  logout() {
    this._session.logout();
  }

  ngOnInit() {
    this._session.User$.subscribe(user => this.user = user);
    // const params = new HttpParams()
    //   .append("username", "flavian")
    //   .append("password", "Test1234=");
      
    // this._http.get("http://localhost:3000/users", {params}).subscribe(data => {
    //   console.log(data);
    // })
  }
}
