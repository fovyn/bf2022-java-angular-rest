import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'enfant-list',
  templateUrl: './enfant-list.component.html',
  styleUrls: ['./enfant-list.component.scss']
})
export class EnfantListComponent implements OnInit {

  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
    this._http.get<any[]>("http://localhost:3000/enfants").subscribe(data => console.log(data));
  }

}
