import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnfantsRoutingModule } from './enfants-routing.module';
import { EnfantsComponent } from './enfants.component';
import { EnfantListComponent } from './components/enfant-list/enfant-list.component';


@NgModule({
  declarations: [
    EnfantsComponent,
    EnfantListComponent
  ],
  imports: [
    CommonModule,
    EnfantsRoutingModule
  ]
})
export class EnfantsModule { }
