import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnfantsComponent } from './enfants.component';

const routes: Routes = [
  { path: '', component: EnfantsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnfantsRoutingModule { }
