import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TuteurDetailComponent } from './components/tuteur-detail/tuteur-detail.component';
import { TuteurComponent } from './tuteur.component';

const routes: Routes = [
  { path: '', component: TuteurComponent, children: [
    { path: ":id", component: TuteurDetailComponent } // /tuteur/1
  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TuteurRoutingModule { }
