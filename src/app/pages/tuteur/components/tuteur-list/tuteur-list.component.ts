import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Tuteur } from '../../models/tuteur.dto';

@Component({
  selector: 'app-tuteur-list',
  templateUrl: './tuteur-list.component.html',
  styleUrls: ['./tuteur-list.component.scss']
})
export class TuteurListComponent implements OnInit {
  private getAll$: Observable<any[]>
  private tuteurs: Tuteur[] = [];
  
  get Tuteurs(): Tuteur[] { return this.tuteurs; }

  constructor(private _http: HttpClient) {    
    this.getAll$ = this._http.get<Tuteur[]>("http://localhost:3000/tuteurs");

    // this._http.post<any>("http://localhost:8080/user/login", {username: 'admin', password: '1234'}).subscribe(data => console.log(data))
  }

  ngOnInit(): void {
    this.getAll$.subscribe(data => {
      this.tuteurs = [...data]
      console.log(data);
    });
  }

}
