import { HttpClient, HttpEvent, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Tuteur } from '../../models/tuteur.dto';

@Component({
  selector: 'app-tuteur-detail',
  templateUrl: './tuteur-detail.component.html',
  styleUrls: ['./tuteur-detail.component.scss']
})
export class TuteurDetailComponent implements OnInit {
  private getOne$: Observable<Tuteur> | null = null;
  private tuteur: Tuteur | null = null;
  get Tuteur(): Tuteur | null { return this.tuteur; }

  constructor(private _http: HttpClient, private _route: ActivatedRoute) {
    this._route.paramMap.subscribe(map => {
      this.getOne$ = this._http.get<Tuteur>(`http://localhost:8080/tuteur/${map.get("id")}`)
      this.getOne$.subscribe(tuteur => this.tuteur = tuteur);
    })
  }

  ngOnInit(): void {

  }

}
