import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './modules/security/components/register/register.component';
import { SignInComponent } from './modules/security/components/sign-in/sign-in.component';

const routes: Routes = [
  { path: 'tuteur', loadChildren: () => import('./pages/tuteur/tuteur.module').then(m => m.TuteurModule) },
  { path: 'enfants', loadChildren: () => import('./pages/enfants/enfants.module').then(m => m.EnfantsModule) },
  { path: 'sign-in', component: SignInComponent },
  { path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
